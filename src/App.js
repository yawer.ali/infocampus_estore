import React from 'react';
import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Home from './components/Home';
import MyCart from './components/Cart'
import ManageProduct from './components/Manageproduct'
import ManageOrder from './components/order';
import Account from './components/account';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home/>}/>
        <Route exact path="/Cart" element={<MyCart/>}/>
        <Route exact path="/account" element={<Account/>}/>
        <Route exact path="/myproduct" element={<ManageProduct/>}/>
        <Route exact path="/myorder" element={<ManageOrder/>}/>
      </Routes>
    </BrowserRouter>
  );
 
}

export default App;
