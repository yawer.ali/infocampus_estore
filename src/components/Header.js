import React from 'react'
import { Link } from 'react-router-dom'

export default function MyHeader() {
  return (
    <div className="container mt-3 ">
        <div className="row ">
            <div className="col-lg-4 text-center text-success">
                <i className='fa fa-shopping-cart fa-3x color-dark'>shopping</i>
            </div>

            <div className="col-lg-8 text-end">
                <div className="btn-group">
                    <Link className='btn btn-primary pe-4 ps-4' to="/">
                       <i className='fa fa-home'></i> Home</Link>

                    <Link className='btn btn-primary pe-4 ps-4' to="/"><i className='fa fa-suitcase p-1'></i>Shopping</Link>

                    <Link className='btn btn-primary pe-4 ps-4' to="/Cart"><i className='fa fa-shopping-cart'></i>My Cart</Link>
                    <Link className='btn btn-primary pe-4 ps-4' to="/Account"><i className='fa fa-user'></i>Vendor</Link>
                </div>
            </div>
        </div>
    </div>
  )
}
