
import React from 'react'
import { Link } from 'react-router-dom'


export default function AdminHeader() {
  return (
    <div className="container mt-3">
        <div className="row">
            <div className="col-lg-4 text-center text-success">
                <i className='fa fa-shopping-cart fa-3x color-dark '>shopping</i>
            </div>

            <div className="col-lg-8 text-end">
                <div className="btn-group">
                    <Link className='btn btn-primary pe-4 ps-4' to="/myproduct">
                       <i className='fa fa-home'></i> Manage Product</Link>

                    <Link className='btn btn-primary pe-4 ps-4' to="/myorder"><i className='fa fa-suitcase p-1'></i>Manage Orders</Link>

                    <button className='btn btn-primary pe-4 ps-4'>
                    Welcome  {localStorage.getItem("name")} <i className='fa fa-power-off text-danger' onClick={logout}> Lagout </i> </button>
                </div>
            </div>

        </div>
    </div>
  )
}

const logout = ()=>{
    localStorage.clear();
    window.location.href="http://localhost:3000/#/account";
}
